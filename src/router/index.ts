import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Login from "../views/Login.vue";
import Rooms from "../views/Rooms.vue";
import Game from "../views/Game.vue";
import info from "../utils/info.util";
import { VueEasyJwt } from "vue-easy-jwt";
const jwt = new VueEasyJwt();


Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    redirect: "/login"
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: {
      hideHeader: true,
      inGame: false,
      hidePlayerLetters: true,
    }
  },
  {
    path: "/rooms",
    name: "Rooms",
    component: Rooms,
    meta: {
      hideHeader: false,
      inGame: false,
      hidePlayerLetters: true,
    }
  },
  {
    path: "/game/:room_id",
    name: "Game",
    component: Game,
    meta: {
      hideHeader: false,
      inGame: true,
      hidePlayerLetters: false,
    }
  },
];

const router = new VueRouter({
  mode: process.env.VUE_APP_IS_ELECTRON ? "hash" : "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  to.matched.some(route => {
    if (info.getToken() !== undefined) {
      const token: string = info.getToken();
      if (jwt.isExpired(token)) {
        localStorage.removeItem("info");
        next({ path: "/login" });
      } else {
        next();
      }
    } else {
      if(route.path == "/login"){
        next();
      }else{
        next({ path: "/login" });
      }
    }
  });
});

export default router;
