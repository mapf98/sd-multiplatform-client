import { API_URL } from "../service.config";
import info from "../../utils/info.util";

export default {
  createRoom(iso_language_code: string) {
    return API_URL.post(`/games/create`, {iso_code: iso_language_code, user_alias: info.getAlias()}, {
      headers: {
        Authorization: `Bearer ${info.getToken()}`
      }
    });
  },
};
