export interface User {
  user_id: number;
  email: string;
  alias: string;
  gender: string;
  first_name: string;
  last_name: string;
}
