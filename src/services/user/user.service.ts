import { API_URL } from "../service.config";
import info from "../../utils/info.util";

export default {
  logIn(user: any) {
    return API_URL.post(`/login`, user);
  },
};
