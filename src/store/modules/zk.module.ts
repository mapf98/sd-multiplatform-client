import zkService from "@/services/zk/zk.service";
import Vue from "vue";
import { ZKState } from "./states/zk.state";
import ZKService from "../../services/zk/zk.service";

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    createRoom: async (context: any, payload: any) => {
      const response = await ZKService.createRoom(payload);

      if (response.data.error) {
        return { error: true, message: response.data.error };
      } else {
        return response.data;
      }
    },
  }
};
