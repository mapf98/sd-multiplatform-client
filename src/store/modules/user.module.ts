import Vue from "vue";
import { UserState } from "./states/user.state";
import UserService from "../../services/user/user.service";
import { User } from "@/services/user/entities/user.interface";

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    logIn: async (context: any, payload: any) => {
      const response = await UserService.logIn(payload);
      const info = {
        token: response.data.access_token,
        alias: payload.user.username
      };
      localStorage.setItem("info", JSON.stringify(info));

      if (response.data.error) {
        return { error: true, message: response.data.error };
      } else {
        return { error: false, message: "" };
      }
    },
  }
};
