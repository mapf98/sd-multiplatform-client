import Vue from "vue";
import { BoardState } from "./states/board.state";

export default {
  namespaced: true,
  state: {
    zk_board: [],
    zk_status: "",
    zk_pass: 0,
    zk_users: [],
    zk_turn: "",
    zk_bag: [],
    zk_verify: 0,
    zk_language: "ES",
  },
  getters: {
    getZKBoard: (state: BoardState) => state.zk_board,
    getZKStatus: (state: BoardState) => state.zk_status,
    getZKPass: (state: BoardState) => state.zk_pass,
    getZKUsers: (state: BoardState) => state.zk_users,
    getZKTurn: (state: BoardState) => state.zk_turn,
    getZKBag: (state: BoardState) => state.zk_bag,
    getZKVerify: (state: BoardState) => state.zk_verify,
    getZKLanguage: (state: BoardState) => state.zk_language,
  },
  mutations: {
    setZKBoard(state: BoardState, zk_board: Array<any>) {
      Vue.set(state, "zk_board", zk_board);
    },
    setZKStatus(state: BoardState, zk_status: string) {
      Vue.set(state, "zk_status", zk_status);
    },
    setZKPass(state: BoardState, zk_pass: number) {
      Vue.set(state, "zk_pass", zk_pass);
    },
    setZKUsers(state: BoardState, zk_users: Array<any>) {
      Vue.set(state, "zk_users", zk_users);
    },
    setZKTurn(state: BoardState, zk_turn: string) {
      Vue.set(state, "zk_turn", zk_turn);
    },
    setZKBag(state: BoardState, zk_bag: Array<any>) {
      Vue.set(state, "zk_bag", zk_bag);
    },
    setZKVerify(state: BoardState, zk_verify: number) {
      Vue.set(state, "zk_verify", zk_verify);
    },
    setZKLanguage(state: BoardState, zk_language: string) {
      Vue.set(state, "zk_language", zk_language);
    },
  },
  actions: {
    setZKBoard: async (context: any, payload: Array<any>) => {
      context.commit("setZKBoard", payload);
    },
    setZKStatus: async (context: any, payload: string) => {
      context.commit("setZKStatus", payload);
    },
    setZKPass: async (context: any, payload: number) => {
      context.commit("setZKPass", payload);
    },
    setZKUsers: async (context: any, payload: Array<any>) => {
      context.commit("setZKUsers", payload);
    },
    setZKTurn: async (context: any, payload: string) => {
      context.commit("setZKTurn", payload);
    },
    setZKBag: async (context: any, payload: Array<any>) => {
      context.commit("setZKBag", payload);
    },
    setZKVerify: async (context: any, payload: number) => {
      context.commit("setZKVerify", payload);
    },
    setZKLanguage: async (context: any, payload: number) => {
      context.commit("setZKLanguage", payload);
    },
  }
};
