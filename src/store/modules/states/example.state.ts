import { Example } from "../../../services/example/entities/example.interface";

export interface ExampleState {
  examples?: Array<Example>;
}
