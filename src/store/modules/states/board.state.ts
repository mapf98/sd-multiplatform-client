export interface BoardState {
  zk_board?: {
    x: number;
    y: number;
    letter: { symbol: string; id: number; value: number };
    user: string;
  }[];
  zk_status?: string;
  zk_pass?: number;
  zk_users?:  Array<any>;
  zk_turn?: string;
  zk_bag?:  Array<any>;
  zk_verify?: number;
  zk_language?: string;
}
