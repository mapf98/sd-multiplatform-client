import Vue from "vue";
import Vuex from "vuex";

import example from "./modules/example.module";
import board from "./modules/board.module";
import user from "./modules/user.module";
import zk from "./modules/zk.module";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    example,
    board,
    user,
    zk
  }
});
