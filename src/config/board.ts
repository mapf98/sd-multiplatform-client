export default {
  BOARD: [
    //ROW 1
    {
      row: [
        {
          cell: {
            x: 1,
            y: 1,
            multiplicator: 3,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 1,
            y: 2,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 1,
            y: 3,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 1,
            y: 4,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 1,
            y: 5,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 1,
            y: 6,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 1,
            y: 7,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 1,
            y: 8,
            multiplicator: 3,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 1,
            y: 9,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 1,
            y: 10,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 1,
            y: 11,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 1,
            y: 12,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 1,
            y: 13,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 1,
            y: 14,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 1,
            y: 15,
            multiplicator: 3,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
      ],
      id: 1
    },
    //ROW 2
    {
      row: [
        {
          cell: {
            x: 2,
            y: 1,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 2,
            y: 2,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 2,
            y: 3,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 2,
            y: 4,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 2,
            y: 5,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 2,
            y: 6,
            multiplicator: 3,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 2,
            y: 7,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 2,
            y: 8,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 2,
            y: 9,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 2,
            y: 10,
            multiplicator: 3,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 2,
            y: 11,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 2,
            y: 12,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 2,
            y: 13,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 2,
            y: 14,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 2,
            y: 15,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
      ],
      id: 2
    },
    //ROW 3
    {
      row: [
        {
          cell: {
            x: 3,
            y: 1,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 3,
            y: 2,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 3,
            y: 3,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 3,
            y: 4,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 3,
            y: 5,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 3,
            y: 6,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 3,
            y: 7,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 3,
            y: 8,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 3,
            y: 9,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 3,
            y: 10,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 3,
            y: 11,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 3,
            y: 12,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 3,
            y: 13,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 3,
            y: 14,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 3,
            y: 15,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
      ],
      id: 3
    },
    //ROW 4
    {
      row: [
        {
          cell: {
            x: 4,
            y: 1,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 4,
            y: 2,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 4,
            y: 3,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 4,
            y: 4,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 4,
            y: 5,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 4,
            y: 6,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 4,
            y: 7,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 4,
            y: 8,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 4,
            y: 9,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 4,
            y: 10,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 4,
            y: 11,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 4,
            y: 12,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 4,
            y: 13,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 4,
            y: 14,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 4,
            y: 15,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
      ],
      id: 4
    },
    //ROW 5
    {
      row: [
        {
          cell: {
            x: 5,
            y: 1,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 5,
            y: 2,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 5,
            y: 3,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 5,
            y: 4,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 5,
            y: 5,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 5,
            y: 6,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 5,
            y: 7,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 5,
            y: 8,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 5,
            y: 9,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 5,
            y: 10,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 5,
            y: 11,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 5,
            y: 12,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 5,
            y: 13,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 5,
            y: 14,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 5,
            y: 15,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
      ],
      id: 5
    },
    //ROW 6
    {
      row: [
        {
          cell: {
            x: 6,
            y: 1,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 6,
            y: 2,
            multiplicator: 3,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 6,
            y: 3,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 6,
            y: 4,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 6,
            y: 5,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 6,
            y: 6,
            multiplicator: 3,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 6,
            y: 7,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 6,
            y: 8,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 6,
            y: 9,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 6,
            y: 10,
            multiplicator: 3,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 6,
            y: 11,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 6,
            y: 12,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 6,
            y: 13,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 6,
            y: 14,
            multiplicator: 3,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 6,
            y: 15,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
      ],
      id: 6
    },
    //ROW 7
    {
      row: [
        {
          cell: {
            x: 7,
            y: 1,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 7,
            y: 2,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 7,
            y: 3,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 7,
            y: 4,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 7,
            y: 5,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 7,
            y: 6,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 7,
            y: 7,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 7,
            y: 8,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 7,
            y: 9,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 7,
            y: 10,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 7,
            y: 11,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 7,
            y: 12,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 7,
            y: 13,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 7,
            y: 14,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 7,
            y: 15,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
      ],
      id: 7
    },
    //ROW 8
    {
      row: [
        {
          cell: {
            x: 8,
            y: 1,
            multiplicator: 3,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 8,
            y: 2,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 8,
            y: 3,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 8,
            y: 4,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 8,
            y: 5,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 8,
            y: 6,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 8,
            y: 7,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 8,
            y: 8,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: true,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 8,
            y: 9,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 8,
            y: 10,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 8,
            y: 11,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 8,
            y: 12,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 8,
            y: 13,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 8,
            y: 14,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 8,
            y: 15,
            multiplicator: 3,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
      ],
      id: 8
    },
    //ROW 9
    {
      row: [
        {
          cell: {
            x: 9,
            y: 1,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 9,
            y: 2,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 9,
            y: 3,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 9,
            y: 4,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 9,
            y: 5,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 9,
            y: 6,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 9,
            y: 7,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 9,
            y: 8,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 9,
            y: 9,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 9,
            y: 10,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 9,
            y: 11,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 9,
            y: 12,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 9,
            y: 13,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 9,
            y: 14,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 9,
            y: 15,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
      ],
      id: 9
    },
    //ROW 10
    {
      row: [
        {
          cell: {
            x: 10,
            y: 1,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 10,
            y: 2,
            multiplicator: 3,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 10,
            y: 3,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 10,
            y: 4,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 10,
            y: 5,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 10,
            y: 6,
            multiplicator: 3,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 10,
            y: 7,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 10,
            y: 8,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 10,
            y: 9,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 10,
            y: 10,
            multiplicator: 3,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 10,
            y: 11,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 10,
            y: 12,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 10,
            y: 13,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 10,
            y: 14,
            multiplicator: 3,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 10,
            y: 15,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
      ],
      id: 10
    },
    //ROW 11
    {
      row: [
        {
          cell: {
            x: 11,
            y: 1,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 11,
            y: 2,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 11,
            y: 3,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 11,
            y: 4,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 11,
            y: 5,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 11,
            y: 6,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 11,
            y: 7,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 11,
            y: 8,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 11,
            y: 9,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 11,
            y: 10,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 11,
            y: 11,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 11,
            y: 12,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 11,
            y: 13,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 11,
            y: 14,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 11,
            y: 15,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
      ],
      id: 11
    },
    //ROW 12
    {
      row: [
        {
          cell: {
            x: 12,
            y: 1,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 12,
            y: 2,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 12,
            y: 3,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 12,
            y: 4,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 12,
            y: 5,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 12,
            y: 6,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 12,
            y: 7,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 12,
            y: 8,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 12,
            y: 9,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 12,
            y: 10,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 12,
            y: 11,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 12,
            y: 12,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 12,
            y: 13,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 12,
            y: 14,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 12,
            y: 15,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
      ],
      id: 12
    },
    //ROW 13
    {
      row: [
        {
          cell: {
            x: 13,
            y: 1,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 13,
            y: 2,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 13,
            y: 3,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 13,
            y: 4,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 13,
            y: 5,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 13,
            y: 6,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 13,
            y: 7,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 13,
            y: 8,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 13,
            y: 9,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 13,
            y: 10,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 13,
            y: 11,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 13,
            y: 12,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 13,
            y: 13,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 13,
            y: 14,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 13,
            y: 15,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
      ],
      id: 13
    },
    //ROW 14
    {
      row: [
        {
          cell: {
            x: 14,
            y: 1,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 14,
            y: 2,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 14,
            y: 3,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 14,
            y: 4,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 14,
            y: 5,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 14,
            y: 6,
            multiplicator: 3,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 14,
            y: 7,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 14,
            y: 8,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 14,
            y: 9,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 14,
            y: 10,
            multiplicator: 3,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 14,
            y: 11,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 14,
            y: 12,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 14,
            y: 13,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 14,
            y: 14,
            multiplicator: 2,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 14,
            y: 15,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
      ],
      id: 14
    },
    //ROW 15
    {
      row: [
        {
          cell: {
            x: 15,
            y: 1,
            multiplicator: 3,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 15,
            y: 2,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 15,
            y: 3,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 15,
            y: 4,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 15,
            y: 5,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 15,
            y: 6,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 15,
            y: 7,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 15,
            y: 8,
            multiplicator: 3,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 15,
            y: 9,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 15,
            y: 10,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 15,
            y: 11,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 15,
            y: 12,
            multiplicator: 2,
            multiplicatorType: "letter",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 15,
            y: 13,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 15,
            y: 14,
            multiplicator: 1,
            multiplicatorType: null,
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
        {
          cell: {
            x: 15,
            y: 15,
            multiplicator: 3,
            multiplicatorType: "word",
            isAvailable: false,
            isBorder: false,
            value: null
          }
        },
      ],
      id: 15
    },
  ],
}
