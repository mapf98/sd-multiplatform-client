export default {
  getToken: () => {
    const data = localStorage.getItem("info");
    const token = JSON.parse(data !== null ? data : "{}").token;
    return token;
  },
  getAlias: () => {
    const data = localStorage.getItem("info");
    const alias = JSON.parse(data !== null ? data : "{}").alias;
    return alias;
  }
};
