export class ZKResponse {
  constructor(
    public error: any,
    public errorMsg: string,
    public stat: any,
    public data: any
  ) {}
}
