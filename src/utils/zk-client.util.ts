import { ZKResponse } from "./entities/zk.response";
const zookeeper = require("node-zookeeper-client");
const ZookeeperWatcher = require('zookeeper-watcher');

import { ZookeeperLock } from "zk-lock";
import { SimpleLocator } from 'locators';
const simpleLocator = SimpleLocator.getLocatorFactory();

let zkClient: any;
let zkWatcher: any;

function getZKLock(){
  return new ZookeeperLock(
    {serverLocator: simpleLocator(`${process.env.VUE_APP_ZK_HOST}:${process.env.VUE_APP_ZK_HOST_PORT}`),
    pathPrefix: "",
    sessionTimeout: 30000,
    spinDelay: 2000,
    retries: 6,
    failImmediate: false,
    maxConcurrentHolders: 1
  });;
}

function connect(server: string) {
  return new Promise((resolve, reject) => {
    let timeout = false;
    const client = zookeeper.createClient(server);
    client.once("connected", () => {
      if (timeout) return;
      timeout = true;
      resolve(client);
    });
    client.connect();

    setTimeout(() => {
      if (!timeout) {
        reject();
      }
    }, 5000);
  });
}

function connectWatcher(server: string) {
  return new Promise((resolve, reject) => {
    let timeout = false;
    const watcher = new ZookeeperWatcher({
      hosts: [server],
      root: '',
      // reconnectTimeout: 20000, // default auto reconnect timeout: 20 seconds
    });

    watcher.once('connected', function (err: any) {
      if (timeout) return;
      timeout = true;
      resolve(watcher);
    });

    setTimeout(() => {
      if (!timeout) {
        reject();
      }
    }, 5000);
  });
}

async function connectZK() {
  zkClient = await connect(
    `${process.env.VUE_APP_ZK_HOST}:${process.env.VUE_APP_ZK_HOST_PORT}`
  );
}

async function connectZKWatcher() {
  zkWatcher = await connectWatcher(
    `${process.env.VUE_APP_ZK_HOST}:${process.env.VUE_APP_ZK_HOST_PORT}`
  );
}

function getZKClient() {
  return zkClient;
}

function getZKWatcher() {
  return zkWatcher;
}

async function getData(path: string) {
  return new Promise((resolve, reject) => {
    zkClient.getData(path, (error: any, data: any, stat: any) => {
      if (error) {
        console.log(error);
        reject(new ZKResponse(true, error.message, stat, ""));
      }

      resolve(new ZKResponse(false, "", stat, data.toString("utf8")));
    });
  });
}

async function checkNodeExist(path: string): Promise<boolean> {
  return new Promise((resolve, reject) => {
    zkClient.exists(path, (error: any, stat: any) => {
      if (error) {
        console.log(error);
        reject(new ZKResponse(true, error.message, stat, ""));
      }
  
      if (stat) {
        resolve(true);
      } else {
        resolve(false);
      }
    });
  });
}

async function setData(path: string, data: string) {
  const bufferData = Buffer.from(data, 'utf8');
  return new Promise((resolve, reject) => {
    zkClient.setData(path, bufferData, function(error: any, stat: any) {
      if (error) {
        reject(new ZKResponse(true, error.message, stat, ""));
      }

      resolve(new ZKResponse(false, "", stat, "setted"));
    });
  });
}

async function getZKNodes(path: string) {
  return new Promise((resolve, reject) => {
    zkClient.getChildren(path, function (error: any, children: any, stat: any) {
      if (error) {
        reject(new ZKResponse(true, error.message, stat, ""));
      }

      resolve(new ZKResponse(false, "", stat, children));
    });
  });
}

async function createZKNode(path: string) {
  return new Promise((resolve, reject) => {
    zkClient.create(
      path,
      null,
      zookeeper.CreateMode.EPHEMERAL,
      function (error: any, path: any) {
        if (error) {
          reject(new ZKResponse(true, error.message, "", ""));
        }

        resolve(new ZKResponse(false, "", null, `Node: ${path} is created`));
      }
    );
  });
}

async function removeRecursiveZKNode(path: string) {
  return new Promise((resolve, reject) => {
    zkClient.removeRecursive(path, -1, function (error: any) {
      if (error) {
        reject(new ZKResponse(true, error.message, "", ""));
      }
  
      resolve(new ZKResponse(false, "", null, `Node: ${path} is deleted`));
    });
  });
}

export { connectZK, connectZKWatcher, getData, setData, getZKClient, getZKWatcher, getZKNodes, createZKNode, removeRecursiveZKNode, checkNodeExist, getZKLock };
